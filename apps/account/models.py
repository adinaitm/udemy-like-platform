import code
from tabnanny import verbose
from django.db import models
from django.utils.crypto import get_random_string
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager


class UserManager(BaseUserManager):
    
    def _create(self, username, phone, password, **extra_fields):
        if not username and phone:
            raise ValueError('User must have username and phone')
        user = self.model(
            username=username,
            phone=phone,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, phone, password, **extra_fields):
        extra_fields.setdefault('is_active', False)
        extra_fields.setdefault('is_staff', False)
        return self._create(username, phone, password, **extra_fields)

    def create_superuser(self, username, phone, password, **extra_fields):
        extra_fields.setdefault('is_active', True)
        extra_fields.setdefault('is_staff', True)
        return self._create(username, phone, password, **extra_fields)


class User(AbstractBaseUser):
    RANDOM_STRING_CHARS = "1234567890"

    TYPE_CHOICES = (
        ('mentor', 'Ментор'),
        ('user', 'Пользователь')
    )
    EXPERIENCE_CHOICES = (
        ('priv-off', 'Лично, частным образом'),
        ('priv-prof', 'Лично, профессионально'),
        ('online', 'Онлайн'),
        ('other', 'Другое')
    )
    AUDIENCE_CHOICES = (
        ('no', 'В настоящий момент нет'),
        ('small', 'У меня маленькая аудитория'),
        ('large', 'У меня достаточная аудитория')
    )

    email = models.CharField(max_length=255, unique=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    type = models.CharField(max_length=12, choices=TYPE_CHOICES, default=...)
    experience = models.CharField(max_length=25, choices=EXPERIENCE_CHOICES, default=...)
    audience = models.CharField(max_length=30, choices=AUDIENCE_CHOICES, default=...)
    is_mentor = models.BooleanField(default=False)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['type', 'first_name', 'last_name']

    def has_module_perms(self, app_label):
        return self.is_staff

    def has_perm(self, obj=None):
        return self.is_staff

    def __str__(self) -> str:
        return self.email

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'